#!/usr/bin/env python3
from mahlzeit import Mahlzeit

m = Mahlzeit()

m.einkauf(28.62, ('Jann', 'Flo', 'Max'), 'Flo')
m.einkauf(2.22, ('Jann', 'Flo', 'Max'), 'Jann')
m.einkauf(14.24, ('Kai', 'Jann', 'Flo', 'Max'), 'Max')
m.einkauf(2.90, ('Kai', 'Jann', 'Flo', 'Max'), 'Kai')
m.einkauf(18.73, ('Julie', 'Jann', 'Flo', 'Max'), 'Flo')
m.pprint()
m.bezahlung('Max', 'Flo', 5)
m.pprint()
