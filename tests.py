import unittest
import random
import subprocess, io
from mahlzeit import Mahlzeit, Esser as E, MahlzeitException


class TestMahlzeit(unittest.TestCase):

    def setUp(self):
        self.m = Mahlzeit()

    def test_multiple_equal(self):
        m = self.m
        m.einkauf('5', (E('a', 2,), E('b', 3)), ('b', 'b'))
        m.calc()
        self.assertEqual(self.m.calc(), dict(
            a=2,b=-2))

    def test_pretty(self):
        m = self.m
        m.einkauf(7, ('a', 'b', 'c'), 'a')
        f = io.StringIO()
        m.pretty(file=f, places=6)
        m.pretty(file=f, places=0)


class TestEinkauf(unittest.TestCase):

    def setUp(self):
        self.m = Mahlzeit()

    def test_payee_not_in_esser(self):
        self.m.einkauf(20, ('a', 'b'), 'c')
        self.m.einkauf(60, ('a', 'b'), 'd')
        self.assertEqual(self.m.calc(), dict(
            a=40,b=40,c=-20,d=-60))
        self.m.bezahlung('a', 'c', 20)
        self.assertEqual(self.m.calc(), dict(
            a=20,b=40,c=0,d=-60))
        self.m.bezahlung('b', 'd', 40)
        self.assertEqual(self.m.calc(), dict(
            a=20,b=0,c=0,d=-20))

    def test_valid(self):
        self.m.einkauf(20, ('a', 'b'), 'a')
        self.m.einkauf(60, ('a', 'b'), 'b')
        self.assertEqual(self.m.calc(), dict(
            a=20,b=-20))
        self.m.bezahlung('a', 'b', 10)
        self.m.bezahlung(E('a', 1), E('b', 1), 10)
        self.assertEqual(self.m.calc(), dict(
            a=0,b=0))
        self.m.einkauf(10, E('a', 2), E('b', 2))
        self.assertEqual(self.m.calc(), dict(
            a=10,b=-10))

    def test_negative_betrag(self):
        self.m.einkauf(-20, ('a', 'b'), 'c')
        self.assertEqual(self.m.calc(), dict(
            a=-10,b=-10,c=20))

    def test_esser_str(self):
        self.m.einkauf(20, 'a', 'c')
        self.assertEqual(self.m.calc(), dict(
            a=20,c=-20))

    def test_esser_list(self):
        self.m.einkauf(20, ['a', 'b'], 'c')

    def test_reset_bezahlungen(self):
        self.m.bezahlung('a', 'b', 20)
        self.assertEqual(self.m.calc(), dict(
            a=-20,b=20))
        self.m.reset()
        self.assertEqual(self.m.calc(), dict())

    def test_multi_bezahler(self):
        self.m.einkauf(20, ('a',), ('b', 'c'))
        self.assertEqual(self.m.calc(), dict(
            a=20,b=-10,c=-10))

    def test_multi_bezahler_esser(self):
        self.m.einkauf(20, ('a', 'd'), ('b', 'c'))
        self.assertEqual(self.m.calc(), dict(
            a=10,b=-10,c=-10,d=10))

    def test_rounding(self):
        sets = [
            ('a', 'b', 'c'),
            ('a', 'c'),
            ('a', 'b'),
            ('a',),
        ]

        for _ in range(100):
            self.m.einkauf(random.randint(1,100), random.choice(sets), random.choice(random.choice(sets)))
        self.m.calc()

    def test_type(self):
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(10, True, 'Alice')
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(10, 'Alice', True)
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(10, ('Alice', True), 'Bob')
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(10, ('Alice', 'John'), ('Bob', True))

    def test_account_name_spaces(self):
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(10, 'Foo   Bar', 'Baz')
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(10, 'Foo', 'Baz   Bar')
        self.m.einkauf(10, 'Foo', 'Baz Bar')


class TestEsser(unittest.TestCase):

    def setUp(self):
        self.m = Mahlzeit()

    def test_esser(self):
        self.m.einkauf(30, (E('a', 2), 'b'), (E('c', 2), 'd'))
        self.m.einkauf(60, (E('a', 2), 'b'), E('d', 1))
        self.assertEqual(self.m.calc(), dict(
            a=60,b=30,c=-20,d=-70))
        # len(bezahler) must not be < 1
        with self.assertRaises(MahlzeitException):
            self.m.einkauf(60, (E('a', 2), 'b'), E('d', 0.5))

    def test_esser_fraction(self):
        self.m.einkauf(25, (E('a', 1.5), 'b'), 'c')
        self.m.einkauf(50, (E('a', 1.5), 'b'), 'd')
        self.assertEqual(self.m.calc(), dict(
            a=45,b=30,c=-25,d=-50))

    def test_esser_zero_weight(self):
        self.m.einkauf(25, (E('a', 0), 'b'), 'c')
        self.m.einkauf(50, (E('a', 0), 'b'), 'd')
        self.assertEqual(self.m.calc(), dict(
            a=0,b=75,c=-25,d=-50))
        with self.assertRaises(MahlzeitException):
            # we cannot output the journal because of missing dates
            self.m.journal()

    def test_esser_negative_weight(self):
        with self.assertRaises(MahlzeitException):
            E('e', -2.5)


class TestBezahlung(unittest.TestCase):

    def setUp(self):
        self.m = Mahlzeit()

    def test_type(self):
        with self.assertRaises(MahlzeitException):
            self.m.bezahlung(True, 'Alice', 10)
        with self.assertRaises(MahlzeitException):
            self.m.bezahlung('Alice', True, 10)
        self.m.bezahlung((E('Alice', 2), 'Bob'), ('Max', E('Kai', 0.5)), 15)
        self.assertEqual(self.m.calc(), dict(
            Alice=-10,Bob=-5,Max=10,Kai=5))
        # len(bezahlter) must not be < 1
        with self.assertRaises(MahlzeitException):
            self.m.bezahlung((E('Alice', 2), 'Bob'), E('Kai', 0.5), 15)
        # len(bezahler) must not be < 1
        with self.assertRaises(MahlzeitException):
            self.m.bezahlung(E('Alice', 0.5), E('Kai', 1), 15)

    def test_account_name_spaces(self):
        with self.assertRaises(MahlzeitException):
            self.m.bezahlung('Foo   Bar', 'Baz', 10)
        with self.assertRaises(MahlzeitException):
            self.m.bezahlung(('Foo', 'Sp  ace'), 'Baz Bar', 10)


class TestJournal(unittest.TestCase):

    def setUp(self):
        self.m = Mahlzeit()

    def test_journal(self):
        m = self.m
        with m(datum='2022-03-13', description='Food'):
            m.einkauf(40, ('Max', 'Flo'), 'Flogina')
            m.bezahlung('Max', 'Flogina', 10)
        with io.StringIO() as f:
            m.journal(file=f)
            subprocess.run(["hledger", "-f", "-", "balance"], stdout=subprocess.DEVNULL, input=f.read(), check=True)

if __name__ == '__main__':
    unittest.main()
