# Changes in 1.0.0

## New features

- `journal()`, `pretty()`, and `pprint()` now take two optional arguments: file and places.
  `(file=sys.stdout, places=)`.

## Notes

- The constructor for `Esser` now uses `gewichtung=1` as default.
- Test the `journal()` against a real world ledger

## Bugfixes

- Accept instances and tuples of `Esser` for all arguments of `einkauf()` and `bezahlung()`

# Changes in 0.9.8

## Backwards incompatible changes

- Expenses are now written as positive numbers, income as negative.

## New features

- `Mahlzeit.einkauf()` now accepts negative amounts for sharing income e.g. vouchers

# Changes in 0.9.7

## Backwards incompatible changes

- the `Mahlzeit.journal()` method now requires the essential fields `datum` and `description`
  to be set and errors if they are not set.

## New features

- the `journal()` output now uses sub accounts in the form of `NAME:einkauf:{bezahler,esser}`
  and `NAME:bezahlung:bezahl{ter,er}` to be able to differentiate postings.
- `Mahlzeit.einkauf()` now supports a single string as esser. Previously only lists or tuples
  were accepted.
